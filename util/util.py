# -*- coding: utf-8 -*-

"""
Created on 26/05/2021

@author: matthieufuteral-peter
"""


import os
import torch
from logger import logger
from .muse_util import Dictionary, normalize_embeddings


def early_stopping(model, val_loss: list, max_count: int, path_save: str, epoch: int):
    global ref_value, count
    if len(val_loss) == 1:
        logger.info("Save model ... Epoch 1")
        ref_value = val_loss[-1]
        count = 0
        flag = False
        torch.save(model.state_dict(), os.path.join(path_save, "model.pt"))
    else:
        if val_loss[-1] < ref_value:
            ref_value = val_loss[-1]
            count = 0
            flag = False
            logger.info("Save model ... Epoch {}".format(epoch))
            torch.save(model.state_dict(), os.path.join(path_save, "model.pt"))
        else:
            count += 1
            if count == max_count:
                flag = True
            else:
                flag = False
    return flag


def load_model(model, path_model, device):
    state_dict = torch.load(path_model, map_location=device)
    model.load_state_dict(state_dict)
    logger.info("Weights loaded")
    return model


def load_vocab(path):
    with open(path, 'r') as f:
        vocab = f.read().split("\n")
    vocab = {int(v.split("\t")[0]): v.split("\t")[1] for v in vocab}
    vocab[0] = "[PAD]"
    return vocab


def custom_loading(model, params):

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    path_model = os.path.join("results", params.model_exp, "model.pt")
    model = load_model(model, path_model, device)

    src_emb, tgt_emb = model.embedding_X, model.embedding_Y
    mapping = model.adapt_layer

    if params.cuda:
        src_emb.cuda()
        tgt_emb.cuda()
        mapping.cuda()

    # Normalize
    params.src_mean = normalize_embeddings(src_emb.weight.data, params.normalize_embeddings)
    params.tgt_mean = normalize_embeddings(tgt_emb.weight.data, params.normalize_embeddings)

    path_src_vocab = params.path_src_vocab
    path_tgt_vocab = params.path_tgt_vocab

    src_id2word = load_vocab(path_src_vocab)
    src_word2id = {v: k for k, v in src_id2word.items()}

    tgt_id2word = load_vocab(path_tgt_vocab)
    tgt_word2id = {v: k for k, v in tgt_id2word.items()}

    params.src_dico = Dictionary(src_id2word, src_word2id, params.src_lang)
    params.tgt_dico = Dictionary(tgt_id2word, tgt_word2id, params.tgt_lang)

    return src_emb, tgt_emb, mapping
