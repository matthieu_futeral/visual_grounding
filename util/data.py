# -*- coding: utf-8 -*-

"""
Created on 26/05/2021

@author: matthieufuteral-peter
"""

import os
import time
import pdb
import random
import re
import json
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset, DataLoader
import en_core_web_sm, zh_core_web_sm


class load_vatex(Dataset):

    def __init__(self, mode: str):
        super(load_vatex, self).__init__()

        self.mode = mode
        assert self.mode in ["train", "val", "test"], "mode must be 'train', 'val' or 'test'"

        # Path data
        global_path = os.path.join("data", "vatex")
        # path_i3d_features = os.path.join(global_path,
        #                                  "trainval.zip" if self.mode in ["train", "val"] else "public_test.zip")
        if self.mode == "train":
            fname = "vatex_training_v1.0.json"
        elif self.mode == "val":
            fname = "vatex_validation_v1.0.json"
        elif self.mode == "test":
            fname = "vatex_public_test_english_v1.1.json"
        else:
            raise NameError("mode must be 'train', 'val' or 'test'")

        self.path_data = os.path.join(global_path, fname)
        with open(self.path_data, "r") as fj:
            self.dataset = json.load(fj)

        # Load vocabs
        self.en_vocab = {re.sub(r"\n", "", txt.split('\t')[1]): int(txt.split('\t')[0]) for txt in
                         open(os.path.join(global_path, "english_vocab.txt"), 'r')}
        self.en_inverse_vocab = {v: k for k, v in self.en_vocab.items()}

        self.zh_vocab = {re.sub(r"\n", "", txt.split('\t')[1]): int(txt.split('\t')[0]) for txt in
                         open(os.path.join(global_path, "chinese_vocab.txt"), 'r')}
        self.zh_inverse_vocab = {v: k for k, v in self.zh_vocab.items()}

        # Tokenizer
        self.english_tokenizer = en_core_web_sm.load()
        self.chinese_tokenizer = zh_core_web_sm.load()

        # load I3D features
        if self.mode in ["train", "val"]:
            self.path_visual_features = os.path.join(global_path, "val")  # np.load(path_i3d_features)
        elif self.mode == "test":
            self.path_visual_features = os.path.join(global_path, "public_test")
        else:
            raise NameError("mode must be 'train', 'val' or 'test'")

    def __getitem__(self, item):

        clip_id = self.dataset[item]

        video_id = clip_id["videoID"]
        en_caption = random.choice(clip_id["enCap"])
        zh_caption = random.choice(clip_id["chCap"])

        # Text processing
        en_text = torch.LongTensor(self.tokenizer(en_caption, "en"))
        zh_text = torch.LongTensor(self.tokenizer(zh_caption, "zh"))

        # video features
        vid_features = torch.Tensor(np.load(os.path.join(self.path_visual_features, video_id + ".npy"))).squeeze(0)

        return en_text, zh_text, vid_features

    def __len__(self):
        return len(self.dataset)

    def en_token_to_id(self, token):
        try:
            idx = self.en_vocab[str(token)]
        except:
            idx = self.en_vocab["[UNK]"]
        return idx

    def zh_token_to_id(self, token):
        try:
            idx = self.zh_vocab[str(token)]
        except:
            idx = self.zh_vocab["[UNK]"]
        return idx

    def tokenizer(self, text, lang):
        if lang == "en":
            tokens = list(self.english_tokenizer(text.lower()))
            id_tokens = list(map(self.en_token_to_id, tokens))

        elif lang == "zh":
            tokens = list(self.chinese_tokenizer(text))
            id_tokens = list(map(self.zh_token_to_id, tokens))

        else:
            raise NameError("lang must be 'en' or 'zh'")

        return id_tokens

    def collate_fn(self, batch):
        en_texts, zh_texts, vid_features = list(zip(*batch))
        en_texts = pad_sequence(en_texts, padding_value=0, batch_first=True)
        zh_texts = pad_sequence(zh_texts, padding_value=0, batch_first=True)
        vid_features = pad_sequence(vid_features, padding_value=0.0, batch_first=True)
        return en_texts, zh_texts, vid_features


if __name__ == "__main__":

    data = load_vatex(mode="train")
    data_loader = DataLoader(data, batch_size=10, num_workers=10, shuffle=True, collate_fn=data.collate_fn)
    pdb.set_trace()
