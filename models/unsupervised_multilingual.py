# -*- coding: utf-8 -*-

"""
Created on 26/05/2021

@author: matthieufuteral-peter
"""

import os
import pickle
import gzip
import numpy as np
import torch
import torch.nn as nn


def find_length(features):
    batch_size = features.size(0)
    max_length = features.size(1)
    attn_mask = torch.where(features == 0.0, 0, 1)
    batch_idx, l = torch.where(torch.sum(attn_mask, dim=-1) == 0)
    return torch.LongTensor([max_length if idx not in batch_idx else l[batch_idx.tolist().index(idx)]
                             for idx in range(batch_size)])


class visual_grounding(nn.Module):

    def __init__(self, dw: int, di: int, d: int, visual_features_dim=1024):
        super(visual_grounding, self).__init__()

        self.dw = dw
        # Language
        self.init_embeddings()

        # Adapt layer
        self.adapt_layer = nn.Linear(dw, dw, bias=False)

        # Text mapping
        self.text_mapping = text_maxpooling(dw, di, d)

        # Visual mapping
        self.visual_mapping = nn.LSTM(visual_features_dim, d, batch_first=True, bidirectional=True, dropout=0.2)

        # Log softmax loss
        self.log_softmax = nn.LogSoftmax(dim=-1)

    def init_embeddings(self):
        pathX = os.path.join("data", "vatex", "word_vectors", "english_wordvectors.pkl.gz")
        pathY = os.path.join("data", "vatex", "word_vectors", "chinese_wordvectors.pkl.gz")
        weightsX = np.vstack((np.zeros(self.dw), pickle.load(gzip.open(pathX))))
        weightsY = np.vstack((np.zeros(self.dw), pickle.load(gzip.open(pathY))))

        self.embedding_X = nn.Embedding(weightsX.shape[0], self.dw)
        self.embedding_Y = nn.Embedding(weightsY.shape[0], self.dw)

    def text_embedding(self, text, source=True):
        if source:
            return self.embedding_X(text)
        else:
            return self.adapt_layer(self.embedding_Y(text))

    def forward(self, textX, textY, visual_features):
        # Language X
        hX = self.text_embedding(textX, source=True)
        jointX = self.text_mapping(hX)

        # Language Y
        hY = self.text_embedding(textY, source=False)
        jointY = self.text_mapping(hY)

        # Visual x, y
        lengths = find_length(visual_features)
        packed = nn.utils.rnn.pack_padded_sequence(
            visual_features, lengths, batch_first=True, enforce_sorted=False)
        _, (h_n, c_n) = self.visual_mapping(packed)

        vis = h_n[-1]

        return jointX, jointY, vis

    def compute_loss(self, textX, textY, visual_features):

        jointX, jointY, vis = self.forward(textX, textY, visual_features)
        dotX = torch.mm(jointX, vis.T)
        dotY = torch.mm(jointY, vis.T)
        lossX = - self.log_softmax(dotX).diag()
        lossY = - self.log_softmax(dotY).diag()
        return lossX.mean() + lossY.mean()


class text_maxpooling(nn.Module):
    def __init__(self, embedding_dim: int, intermediate_dim: int, joint_dim: int):
        super(text_maxpooling, self).__init__()

        self.linear = nn.Linear(embedding_dim, intermediate_dim)
        self.mapping = nn.Linear(intermediate_dim, joint_dim)
        self.relu = nn.ReLU()

    def forward(self, text):

        h1 = self.relu(self.linear(text))
        # max pooling
        h1 = h1.max(dim=1)[0]
        embedding = self.mapping(h1)
        return embedding

