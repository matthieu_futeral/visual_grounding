# -*- coding: utf-8 -*-

"""
Created on 26/05/2021

@author: matthieufuteral-peter
"""

from .unsupervised_multilingual import visual_grounding

__all__ = ["visual_grounding"]


