# -*- coding: utf-8 -*-

"""
Created on 26/05/2021

@author: matthieufuteral-peter
"""


# Import system package
import os
import pdb
import sys
import argparse

# Torch package
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

# Logger package
from logger import logger
from tqdm.auto import tqdm

# Util
import time
import json
from uuid import uuid4
from util import load_vatex, early_stopping

# Computation
import numpy as np

# Model
from models import visual_grounding


class Loss:
    def __init__(self):
        self.batch_loss = []
        self.epoch_loss = []

    def init_batch(self):
        self.batch_loss = []

    def update_batch(self, loss):
        self.batch_loss.append(loss)

    def update_epoch(self):
        self.epoch_loss.append(np.mean(self.batch_loss))
        self.init_batch()

class func_args:
    def __init__(self, kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


def train(epoch, model, optimizer, train_loader, train_loss, writer, device, args):

    # Setting args
    model.train()

    # Optimizer
    optimizer.zero_grad()

    for batch_idx, (en_texts, zh_texts, vid_features) in tqdm(enumerate(train_loader)):

        if torch.cuda.is_available():
            en_texts = en_texts.to(device)
            zh_texts = zh_texts.to(device)
            vid_features = vid_features.to(device)

        loss = model.compute_loss(en_texts, zh_texts, vid_features)
        if args.orthogonal_constraint:
            loss += torch.norm(torch.mm(model.adapt_layer.weight,
                                        model.adapt_layer.weight.T) - torch.eye(model.dw).cuda(), p="fro")

        loss.backward()

        train_loss.update_batch(loss.data.item())

        if not (batch_idx + 1) % args.log_interval:

            n_iter = batch_idx + (epoch - 1) * len(train_loader)

            logger.info('Batch idx: {} \tLoss: {:.6f}'.format(batch_idx + 1,
                                                              np.mean(train_loss.batch_loss)))

            writer.add_scalar('Loss/train', np.mean(train_loss.batch_loss), n_iter)
            # Summary for tensorboard
            for name, weight in model.named_parameters():
                writer.add_histogram(name, weight, n_iter)
                writer.add_histogram(f'{name}.grad', weight.grad, n_iter)

            writer.add_graph(model, (en_texts, zh_texts, vid_features))

        if not (batch_idx + 1) % args.gradient_step:
            optimizer.step()
            optimizer.zero_grad()

    del en_texts, zh_texts, vid_features  # free memory
    train_loss.update_epoch()

    logger.info("Train loss over Epoch {} : {:.6f}".format(epoch, train_loss.epoch_loss[-1]))

    return model


def test(epoch, model, val_loader, val_loss, writer, device, args):

    model.eval()

    for batch_idx, (en_texts, zh_texts, vid_features) in tqdm(enumerate(val_loader)):

        if torch.cuda.is_available():
            en_texts = en_texts.to(device)
            zh_texts = zh_texts.to(device)
            vid_features = vid_features.to(device)

        with torch.no_grad():
            loss = model.compute_loss(en_texts, zh_texts, vid_features)

        val_loss.update_batch(loss.data.item())

        if not batch_idx % args.log_interval:

            n_iter = batch_idx + (epoch - 1) * len(val_loader)

            logger.info('Batch idx: {} \tLoss: {:.6f}'.format(batch_idx, np.mean(val_loss.batch_loss)))
            writer.add_scalar('Loss/validation', np.mean(val_loss.batch_loss), n_iter)

        del en_texts, zh_texts, vid_features  # free memory

    val_loss.update_epoch()
    logger.info("Validation loss over Epoch {} : {:.6f}".format(epoch, val_loss.epoch_loss[-1]))

    pass


def main(model, optimizer, train_loader, val_loader, path_save, device, args):

    train_loss = Loss()
    val_loss = Loss()
    epoch_time = []
    writer = SummaryWriter(log_dir=path_save + '/runs')

    for epoch in range(1, args.n_epochs + 1):
        logger.info("Epoch {} - Start TRAINING".format(epoch))

        t0 = time.time()  # start time

        # Training step
        model = train(epoch, model, optimizer, train_loader, train_loss, writer, device, args)

        # Validation step
        logger.info("Start evaluating on Validation set...")
        test(epoch, model, val_loader, val_loss, writer, device, args)

        time_elapsed = time.time() - t0
        logger.info("Epoch {} - Time elapsed : {}".format(epoch, time_elapsed))
        epoch_time.append(time_elapsed)

        # Early stopping
        flag = early_stopping(model, val_loss.epoch_loss, max_count=args.patience, path_save=path_save, epoch=epoch)
        if flag:
            print("Stop training... Patience reached")
            break

    logger.info("Average time per epoch : {}".format(np.mean(epoch_time)))
    writer.close()

    return model, train_loss, val_loss, epoch_time


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--n_epochs", type=int, default=100, help="number of epochs")
    parser.add_argument("--batch_size", type=int, default=512, help="size of the batch")
    parser.add_argument("--lr", type=float, default=1e-4, help="learning rate")
    parser.add_argument("--weight_decay", type=float, default=0.0, help="weigth decay")
    parser.add_argument("--debug", action="store_true", help="Use python debogger or not")
    parser.add_argument("--patience", type=int, default=7, help="How many epochs to wait before early stopping")
    parser.add_argument("--gradient_step", type=int, default=1, help="Number of gradient update wait, > 1 to be active")
    parser.add_argument("--log_interval", type=int, default=100, help="log interval")
    parser.add_argument("--exp_name", type=str, required=True, help="Name of the experiment")
    parser.add_argument("--num_workers", type=int, default=16)
    parser.add_argument("--orthogonal_constraint", action="store_true", help="Loss orthogonal constraint")
    args = parser.parse_args()

    if not args.debug:
        pdb.set_trace = lambda: None

    # Generate an id
    RUN_ID = str(uuid4())[0:5]

    # Set device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    device_ids = list(range(torch.cuda.device_count()))
    gpus = len(device_ids)

    logger.info(f"RUN ID : {RUN_ID} - currently running on device : {device}")
    print(vars(args))

    # Dataset
    train_set = load_vatex(mode="train")
    val_set = load_vatex(mode="val")

    # Dataloader
    train_loader = DataLoader(train_set,
                              batch_size=args.batch_size,
                              shuffle=True,
                              collate_fn=train_set.collate_fn,
                              num_workers=args.num_workers)

    val_loader = DataLoader(val_set,
                            batch_size=args.batch_size,
                            shuffle=True,
                            collate_fn=val_set.collate_fn,
                            num_workers=args.num_workers)

    # Tokenizer & Model & Optimizer
    model = visual_grounding(dw=300, di=512, d=300)
    model.to(device)

    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

    # Prepare Saving results
    if torch.cuda.is_available():
        folder_id = os.path.join("results", RUN_ID + "_" + os.environ.get("SLURM_JOB_ID") + "-" + args.exp_name)
    else:
        folder_id = os.path.join("results", RUN_ID + "_" + args.exp_name)
    os.makedirs(folder_id, exist_ok=False)
    results = vars(args)

    # Training
    model, train_loss, val_loss, epoch_time = main(model, optimizer, train_loader, val_loader, folder_id, device, args)

    results['train_loss'] = train_loss.epoch_loss
    results['val_loss'] = val_loss.epoch_loss
    results['epoch_time'] = epoch_time

    with open(os.path.join(folder_id, "results.json"), 'w') as fj:
        json.dump(results, fj)





