# -*- coding: utf-8 -*-

"""
Created on 27/05/2021

@author: matthieufuteral-peter
"""

import os
import gzip
import pickle
import argparse
from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--lang", type=str, required=True)
args = parser.parse_args()

if args.lang == "en":
    path_corpus = os.path.join("data", "vatex", "english_corpus_tokenized.txt")
    path_vocab = os.path.join("data", "vatex", "english_vocab.txt")
elif args.lang == "zh":
    path_corpus = os.path.join("data", "vatex", "chinese_corpus_tokenized.txt")
    path_vocab = os.path.join("data", "vatex", "chinese_vocab.txt")
else:
    raise NameError("args.lang must be 'en' or 'zh'")

# Load data
corpus = LineSentence(path_corpus)

# Model
model = Word2Vec(sentences=corpus, vector_size=300, window=5, negative=5, min_count=3, workers=10, sg=1, epochs=50)

# Save
vocab = [str(idx) + '\t' + word for idx, word in enumerate(model.wv.index_to_key, 1)]
with open(path_vocab, "w") as f:
    f.write("\n".join(vocab))

os.makedirs(os.path.join("data", "vatex", "word_vectors"), exist_ok=True)
if args.lang == "en":
    path_save = os.path.join("data", "vatex", "word_vectors", "english_wordvectors.pkl.gz")
elif args.lang == "zh":
    path_save = os.path.join("data", "vatex", "word_vectors", "chinese_wordvectors.pkl.gz")

vectors = np.asarray(model.wv.vectors)
pickle.dump(vectors, gzip.open(path_save,   'wb'))
